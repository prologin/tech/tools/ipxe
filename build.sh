#!/bin/sh

make -C src -j \
    EMBED=../prologin.ipxe \
    CERT=../certs/ipxe.crt \
    CERT=../certs/isrg-root-x1.crt \
    TRUST=../certs/ipxe.crt,../certs/isrg-root-x1.crt \
    bin/undionly.kpxe \
    bin/ipxe.iso \
    bin-x86_64-efi/ipxe.efi \
    bin-x86_64-efi/snp.efi
