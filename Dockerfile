FROM debian:11 as base

ENV DEBIAN_FRONTEND=noninteractive \
    LC_ALL=C.UTF-8 \
    LANG=C.UTF-8

FROM base as builder

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        gcc \
        make \
        git \
        zlib1g-dev \
        liblzma-dev \
        mkisofs \
        isolinux \
        openssl

RUN mkdir -p /ipxe
WORKDIR /ipxe

COPY ./ ./

RUN ./build.sh

FROM base

RUN apt-get update && \
    apt-get install -y --no-install-recommends tftpd-hpa

RUN mkdir -p /tftp
WORKDIR /tftp

COPY --from=builder /ipxe/src/bin/undionly.kpxe .
COPY --from=builder /ipxe/src/bin/ipxe.iso .
COPY --from=builder /ipxe/src/bin-x86_64-efi/ipxe.efi .
COPY --from=builder /ipxe/src/bin-x86_64-efi/snp.efi .

ENTRYPOINT ["in.tftpd", "--listen", "--foreground", "--secure", "-vvv", "/tftp"]
